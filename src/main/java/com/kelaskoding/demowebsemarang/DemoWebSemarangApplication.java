package com.kelaskoding.demowebsemarang;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoWebSemarangApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoWebSemarangApplication.class, args);
	}

}
